#carga de datos
IP = "postgresbd"
db_name = "reto_forecasting"
user = "gerardo.zarate.bernal@bbva.com"
password = "gzarate1503"

POSTGRESDB_URL = "jdbc:postgresql://" + IP +":5432/"+ db_name +"?user="+ user +"&password="+ password


reto_forecasting = spark.read.\
                jdbc(url=POSTGRESDB_URL, \
                     table='reto_forecasting', \
                     column='id',\
                     lowerBound=1, \
                     upperBound=424048047, \
                     numPartitions=300)


#tipo de variables y datos

reto_forecasting.dtypes

%time reto_forecasting.createOrReplaceTempView('cuenta')
cta=sqlContext.sql('SELECT distinct cod_idcontra FROM cuenta')
cta.count()

#separacion de cargos y abonos por mes

%time abono=reto_forecasting.select('partition_id','cod_ccc','des_man_auto','imp_mov').where("imp_mov>0").cache()
%time cargo=reto_forecasting.select('partition_id','cod_ccc','des_man_auto','imp_mov').where("imp_mov<0").cache()

%time abono1=abono.groupby('partition_id','cod_ccc','des_man_auto').count().cache()
%time cargo1=cargo.groupby('partition_id','cod_ccc','des_man_auto').count().cache()

%time abono2=abono1.withColumnRenamed('count', 'abono').cache()
%time cargo2=cargo1.withColumnRenamed('count', 'cargo').cache()

%time eje1=reto_forecasting.groupby('partition_id','cod_ccc','des_man_auto').count().cache()
%time eje=eje1.select('partition_id','cod_ccc','des_man_auto').cache()

%time tabla1 = eje.join(cargo2, ['partition_id','cod_ccc','des_man_auto'],how='left').cache()
%time tabla = tabla1.join(abono2, ['partition_id','cod_ccc','des_man_auto'],how='left').cache()


# importacion de la tabla sumariazada al libreria pandas#
import pandas as pd
import numpy as np
import math
%matplotlib inline
import matplotlib.pyplot as plt
import matplotlib
from pyspark.sql import functions as F

#transpose manual para la detercion de datos por numero de cuenta

%time tabla.createOrReplaceTempView('mmax')
mmax1=sqlContext.sql('SELECT partition_id as fecha,cod_ccc,\
                        sum(case when cargo is null then 0\
                                 when trim(des_man_auto)="AUTOMATICO" then cargo else 0 end) as cargo_aut,\
                        sum(case when abono is null then 0\
                                 when trim(des_man_auto)="AUTOMATICO" then abono else 0 end) as abono_aut,\
                        sum(case when cargo is null then 0\
                                 when trim(des_man_auto)="MANUAL" then cargo else 0 end) as cargo_man,\
                        sum(case when abono is null then 0\
                                 when trim(des_man_auto)="MANUAL" then abono else 0 end) as abono_man,\
                        sum(case when abono is null then 0 else abono end+case when cargo is null then 0 else cargo end) as total\
                   FROM mmax \
                        group by partition_id,cod_ccc')
						
mmax1.show()

# eliminamos los contratos masivos.

%time mmax1.createOrReplaceTempView('mmax2')
mmax3=sqlContext.sql('SELECT  fecha, cod_pan_tj,cargo_man from mmax2 where cargo_man>2 and trim(cod_pan_tj) not in ("nwdq")')
mmax3.show()

# visualizacion de contratos

time mmax3.createOrReplaceTempView('m1')
m2=sqlContext.sql('SELECT distinct cod_pan_tj from m1')     

%time reto_forecasting.createOrReplaceTempView('m3')
m4=sqlContext.sql('SELECT distinct cod_pan_tj,id from m3')
m2.show()
m4.show()
 
# orden por contrato

%time previo = m2.join(m4, ['cod_pan_tj'],how='left')
%time previo.createOrReplaceTempView('w1')
w1=sqlContext.sql('SELECT * from w1 order by id')
w1.show()

#proceso de discrimanción del ID como variable categorica

%time reto_forecasting.createOrReplaceTempView('m1')
m2=sqlContext.sql('SELECT distinct id,cod_pan_tj from m1 where trim(cod_pan_tj) not in ("nwdq") order by id')

%time m2.createOrReplaceTempView('m3')
m4=sqlContext.sql('SELECT * from m3 where id >= 1000 and  id <= 10000')
m4.show()

# creacion de una variable sintetica para ha clusterizar

tb3 = pd.DataFrame(tb2)
tb4 = spark.createDataFrame(tb3, schema=['fecha','contrato','cargo_aut','abono_aut','cargo_man','abono_man','total','mmc_aut',\
                                            'mmb_aut','mmc_man','mmb_man'])
%time tb4.createOrReplaceTempView('tb5')
tb6=sqlContext.sql('SELECT fecha,contrato, sum(mmc_aut+mmb_aut+mmc_man+mmb_man) as score from tb5 group by fecha,contrato').toPandas()
tb6

# estadisticos de la variable sintetica

rd=tb6
rd.describe()

# determinacion de outliers y limites inferiores

inf=0.004425-((0.027778-0.004425)*1.5)
sup=0.004425+((0.027778-0.004425)*1.5)
sup



# tabla quitando el ruido blanco

pdf = pd.DataFrame(rd)
pdf1 = spark.createDataFrame(pdf, schema=['fecha','contrato','score'])
%time pdf1.createOrReplaceTempView('pdf2')
pdf3=sqlContext.sql('select * from pdf2 where score<=0.03945450').toPandas()
pdf3
				

# visualizacion de la variable en un grafico de bigotes

df=pdf3
df.boxplot()

